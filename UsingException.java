import java.util.*;


public class UsingException
{
	public static void main(String args[])
	{
		try{
			throwException();
		}catch(Exception e){
			System.out.println("Exception Handled in main ");
		}
		doesNotThrowException();
	}

	public static void throwException() throws Exception{
		try
		{
			System.out.println("Method Throw Exception ");
			throw new Exception();
		}catch(Exception e)
		{
			System.err.println("Exception Handled in method throwException");
			throw new Exception();
		}finally{
			System.err.println("finally Executed in throwException");
		}
	}

	public static void doesNotThrowException(){
		try
		{
			System.out.println("Method doesNotThrowException ");
		}catch(Exception e){
			System.err.println(e);
		}finally{
			System.err.println("Finnaly executed in doesNotThrowException");
		}
		System.out.println("End Of Method !");
	}
}