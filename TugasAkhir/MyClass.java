import java.awt.Rectangle;
public class MyClass{

		public static void main(String args[])
		{
			doSomething();
		}

		public static void doSomething()
		{
				try{
						Object object = new Rectangle();
						String string = object.toString();
				}catch(MyException me){
					System.out.println("catching MyException");
				}finally{
						System.out.println("Executing finally block");
				}
		}

}