public class MyException extends ClassCastException
{
	public MyException()
	{
		super("MyException");
	}

	public class MyClass
	{
		public void doSomething(){

			try{
				Object object = new Rectangle();
				String string = (String)object;
			}

		}
	}
}