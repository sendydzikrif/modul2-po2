public class Account
{

	private int accountNumber;
	private int currentBalance;
	public void credit(int amt)
	{
		currentBalance = currentBalance + amt;
	}
	public void debit(int amt)
	{
		int tempBalance = currentBalance - amt;
		if(tempBalance <0 )
		{
			int i = 1;
			System.out.println(i);
		}
		currentBalance = tempBalance;
	}

	public Account(int x, int y)
	{
		this.accountNumber = x;
		this.currentBalance = y;
	}

	public int getCurrentBalance(){
		return this.currentBalance;
	}
	
}