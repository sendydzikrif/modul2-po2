import java.util.*;
import java.io.*;
public class DivideByZeroByExceptionHandling
{
	public static int pembagian(int bil,int pbg)
	{
		return bil/pbg;
	}
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		boolean continueLoop = true;

		do
		{
			try{

				System.out.println("Masukan Numerator ");
				int numerator = sc.nextInt();
				System.out.print("Masukan denominator");
				int denominator = sc.nextInt();

				int result = pembagian(numerator,denominator);
				System.out.printf("%nResult: %d / %d = %d%n", numerator,denominator,result);	
				continueLoop = false;
			}catch(InputMismatchException e){

				System.err.printf("%nException : %s %,",e);
				sc.nextLine();
				System.out.printf("You must input integer");

			}catch(ArithmeticException e){
				System.err.printf("%nException : %s%n",e);
				System.out.println("Zero is invalid denominator ! ");
			}
		}while(continueLoop);
	}
}