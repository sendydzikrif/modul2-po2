import java.awt.Rectangle;
import java.util.*;

public class NullReference
{
	private static Rectangle rectangle;
	public static void main(String[] args)
	{ 
		if(rectangle == null)
		{
			System.out.println("Rectangle variable doesnt refer to Rectangle object ");
		}else{
			int area = rectangle.area();
			System.out.println("Area: " + area);
		}
	}
}