import java.io.*;
import java.util.*;
public class DivideByZero
{
	public static int pembagian(int bil, int pbg)
	{
		return bil/pbg;
	}
	public static void main(String args[])
	{
		Scanner scanner = new Scanner(System.in);

		System.out.print("Masukan Nilai Pembilang : ");
		int numerator = scanner.nextInt();
		System.out.print("Masukan Nilai Pembagi : ");
		int denominator = scanner.nextInt();

		int result = pembagian(numerator,denominator);

		System.out.printf("%nResult: %d / %d = %d%n", numerator,denominator,result);
	}
}